#!/usr/bin/env node
const exec = require('child_process').exec;
const execSync = require('child_process').execSync;
const homedir = require('os').homedir();
const fs = require('fs');
let COLOR_OPT = "--color"
let isAlpine = false
if (fs.existsSync("/etc/alpine-release")) {
  COLOR_OPT = "--color=never";
  isAlpine = true;
}

function runSth(inCmd) {
  exec(inCmd, (err, stdout, stderr) => {
    console.log("[CMD] " + inCmd);
    if (err) {
      console.error(err);
      return;
    }
    console.log(stdout);
  });
}

function runSthSync(inCmd) {
    console.log("[CMD] " + inCmd);
    execSync(inCmd);
}

let cmdList = [
    //user
    "git config --global user.name cfig",
    "git config --global user.email yuyezhong@gmail.com",
    //alias
    "git config --global alias.co checkout",
    "git config --global alias.b branch",
    "git config --global alias.br \"branch -r\"",
    "git config --global alias.ba \"branch -a\"",
    "git config --global alias.ci commit",
    "git config --global alias.st status",
    "git config --global alias.unstage \"reset HEAD --\"",
    "git config --global alias.last \"log -1 HEAD\"",
    `git config --global alias.logs "log ${COLOR_OPT} --graph --decorate=short --all --date-order"`,
    `git config --global alias.l "log ${COLOR_OPT} --graph --decorate=short --date-order --show-signature"`,
    `git config --global alias.ll "log ${COLOR_OPT} --graph --pretty=format:'%Cred%h%Creset - %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"`,
    //core
    "git config --global core.autocrlf false",
    "git config --global core.ignorecase false",
    `git config --global core.excludesfile ${homedir}/.gitignore`,
    "git config --global color.ui auto",
    "git config --global core.editor vim",
    //merge
    "git config --global merge.tool fugitive",
    "git config --global http.sslverify false",
    //gpg sign
    "git config --global commit.gpgsign false",
    "git config --global gpg.program gpg",
    "git config --global user.signingkey D87E652A34641A2EF448CE31D6F140FF1370401F",
    //clang-format
    "git config --global clangFormat.style file",
    "git config --global --path clangFormat.stylePath ~/.clang-format"
];
if (isAlpine) {
    cmdList.push("git config --global color.diff false");
}

cmdList.forEach(aCommand => {
    runSthSync(aCommand);
});

//clang-format from AOSP frameworks/native
let clangFormat = "BasedOnStyle: Google\n" +
    "Standard: Cpp11\n" +
    "AccessModifierOffset: -2\n" +
    "AllowShortFunctionsOnASingleLine: Inline\n" +
    "ColumnLimit: 100\n" +
    "CommentPragmas: NOLINT:.*\n" +
    "DerivePointerAlignment: false\n" +
    "IncludeBlocks: Preserve\n" +
    "IndentWidth: 4\n" +
    "ContinuationIndentWidth: 8\n" +
    "PointerAlignment: Left\n" +
    "TabWidth: 4\n" +
    "UseTab: Never\n"
fs.writeFileSync(`${homedir}/.clang-format`, clangFormat);

