#!/usr/bin/env python3
import urllib.request
import shutil
import os.path
import platform
import subprocess

VIMDIR = os.path.expanduser("~/.vim")
RC_FILE = os.path.expanduser("~/.vimrc")
BUNDLE_DIR = os.path.join(VIMDIR, "bundle")
PLUGIN_DIR = os.path.join(VIMDIR, "plugin")
AUTOLOAD_DIR = os.path.join(VIMDIR, "autoload")

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    def disable(self):
        self.HEADER = ''
        self.OKBLUE = ''
        self.OKGREEN = ''
        self.WARNING = ''
        self.FAIL = ''
        self.ENDC = ''

Xcolors = bcolors();
if "Linux" == platform.system():
    pass
elif "Windows" == platform.system():
    Xcolors.disable()
elif "Darwin" == platform.system():
    pass
else:
    print("Unsupported platform: %s " % platform.system())
    raise

LOG_OK=Xcolors.OKGREEN+ "  [ ok ]" + Xcolors.ENDC
LOG_WARN=Xcolors.WARNING+ "  [warn]" + Xcolors.ENDC
LOG_ERROR=Xcolors.FAIL + "  [error]" + Xcolors.ENDC
LOG_INFO=Xcolors.OKBLUE+ "  [info]" + Xcolors.ENDC

def print_error(theCmd):
    print("%s %s" % (LOG_ERROR, theCmd))
    exit(1)

def print_warn(theCmd):
    print("%s %s" % (LOG_WARN, theCmd))

def print_ok(theCmd):
    print("%s %s" % (LOG_OK, theCmd))

def print_info(theCmd):
    print("%s %s" % (LOG_INFO, theCmd))
def prepare_dir(theDir):
    if os.path.exists(theDir) and not os.path.isdir(theDir):
        os.remove(theDir)
    if not os.path.isdir(theDir):
        os.mkdir(theDir)
    print_ok(theDir + " is ready")

def downFile(src, tgt):
    url = src
    file = tgt
    print("%s %s" % (LOG_OK, "Downloading %s" % src))
    with urllib.request.urlopen(url) as response, open(file, 'wb') as out_file:
        shutil.copyfileobj(response, out_file)

def prepareDirs():
    if (os.path.exists(VIMDIR)):
        print("%s %s" % (LOG_WARN, "unlink %s" % VIMDIR))
        shutil.rmtree(VIMDIR)
    prepare_dir(VIMDIR)
    prepare_dir(BUNDLE_DIR)
    prepare_dir(PLUGIN_DIR)
    prepare_dir(AUTOLOAD_DIR)

def downloadPlugins():
    baseUrl = "https://bitbucket.org/cfig/linux_tools/raw/master/install_vim_plugins"
    # rc
    downFile("%s/vimrc" % baseUrl, "vimrc")
    # single
    downFile("%s/pathogen.vim" % baseUrl, "pathogen.vim") # instead of "https://tpo.pe/pathogen.vim"
    downFile("http://cscope.sourceforge.net/cscope_maps.vim", "cscope_maps.vim")
    downFile("%s/fzf.vim" % baseUrl, "fzf.vim") # instead of https://raw.githubusercontent.com/junegunn/fzf/master/plugin/fzf.vim
    # zip
    downFile("http://www.vim.org/scripts/download_script.php?src_id=18080", "vim-template-0.3.0.zip")

def clonePlugin(remoteName, localName):
    theCmd = "git clone %s ~/.vim/bundle/%s --depth=1" % (remoteName, localName)
    subprocess.check_call(theCmd, shell = True)

def installPlugins():
    #zip
    shutil.unpack_archive("vim-template-0.3.0.zip", os.path.join(BUNDLE_DIR, "vim-template"))
    #.vim
    shutil.move("pathogen.vim", AUTOLOAD_DIR)
    shutil.move("cscope_maps.vim", PLUGIN_DIR)
    shutil.move("fzf.vim", AUTOLOAD_DIR)
    clonePlugin("https://github.com/udalov/kotlin-vim", "kotlin-vim")
    clonePlugin("https://github.com/tfnico/vim-gradle", "vim-gradle")
    clonePlugin("https://github.com/airblade/vim-gitgutter.git", "vim-gitgutter")
    clonePlugin("https://github.com/yegappan/mru.git", "mru")
    clonePlugin("https://github.com/vim-scripts/DrawIt", "DrawIt")
    clonePlugin("https://github.com/majutsushi/tagbar.git",  "tagbar")
    clonePlugin("https://github.com/tpope/vim-fugitive.git", "vim-fugitive")
    clonePlugin("https://github.com/lifepillar/vim-solarized8.git", "vim-solarized8")
    #clonePlugin("https://github.com/ctrlpvim/ctrlp.vim.git", "ctrlp.vim")
    clonePlugin("https://github.com/mileszs/ack.vim.git", "ack.vim")
    clonePlugin("https://github.com/junegunn/fzf.vim.git", "fzf.vim")
    clonePlugin("https://github.com/vim-airline/vim-airline.git", "vim-airline")
    clonePlugin("https://github.com/vim-airline/vim-airline-themes.git", "vim-airline-themes")

def installVimRc():
    if os.path.exists(RC_FILE):
        print("%s %s" % (LOG_WARN, "unlink %s" % RC_FILE))
        os.unlink(RC_FILE)
    print("%s %s" % (LOG_OK, "install config file at %s" % RC_FILE))
    shutil.move("vimrc", RC_FILE)

def cleanUp():
    os.remove("vim-template-0.3.0.zip")
    print("%s %s" % (LOG_OK, "cleaning temperary files"))

if __name__ == "__main__":
    prepareDirs()
    downloadPlugins()
    installPlugins()
    installVimRc()
    cleanUp()
