#include <sys/capability.h>
#include <sys/types.h>
#include <unistd.h>
#include <cstdio>
#include <cstdlib>
#include <cerrno>
#include <sstream>
#include <cstring>
#include <dirent.h>
#include <sys/stat.h>
#include <ftw.h>
#ifdef __ANDROID__
#define LOG_TAG "XX"
#include <utils/Log.h>
#else
#define ALOGE printf
#define ALOGI printf
#endif

std::string capToString(cap_t inCap);
void getProcessCap(pid_t inPid);
void getProcessCap();
void assertRegularFile(const char* fname);
void usage();

void usage() {
    char* helpInfo = (char*) "\
    <prog> 1 <file>            : get file cap\n\
    <prog> 2 <pid>             : get process cap\n\
    <prog> 2                   : get current process cap\n\
    <prog> 3 <file> <cap_text> : set file cap\n\
    <prog> 4 <cap_text>        : set current process cap\n\
";
    ALOGI("Usage:\n%s\n", helpInfo);
}

void die(std::string msg) {
    ALOGE("prog died: %s", msg.c_str());
    abort();
}

bool isFlagCapable(cap_t inCap, cap_value_t inCapValue, cap_flag_t inFlag) {
    cap_flag_value_t value;
    if (cap_get_flag(inCap, inCapValue, inFlag, &value) != 0) {
        ALOGE("cap_get_flag(cap_value_t = %d) failed, errno=%d (%s)\n", inCapValue, errno, strerror(errno));
        die("cap_get_flag failed\n");
    }
    return (value == CAP_SET);
}

void testSetFlat(cap_t inCap) {
    cap_value_t cap_list[1] = { CAP_CHOWN };
    /* effetive cap */
    if (cap_set_flag(inCap, CAP_EFFECTIVE, 1, cap_list, CAP_SET) == -1) {
        ALOGE("cap_set_flag(E) cap_chown: code=%d, str=%s\n", errno, strerror(errno));
        die("cap_set_flag failed\n");
    }
    /* permitted cap */
    if (cap_set_flag(inCap, CAP_PERMITTED, 1, cap_list, CAP_SET) == -1) {
        ALOGE("cap_set_flag(P) cap_chown: code=%d, str=%s\n", errno, strerror(errno));
        die("cap_set_flag failed\n");
    }
    /* inheritable cap */
    if (cap_set_flag(inCap, CAP_INHERITABLE, 1, cap_list, CAP_SET) == -1) {
        ALOGE("cap_set_flag(I) cap_chown: code=%d, str=%s\n", errno, strerror(errno));
        die("cap_set_flag failed\n");
    }
}

void dumpCap(cap_t inCap) {
    ALOGI("  : %-20s:  %s  %s  %s\n", "capability name", "E", "I", "P");
    ALOGI("-----------------------------------\n");
    for (unsigned i=0; i <= CAP_LAST_CAP; i++) {
        char *name_ptr;
        name_ptr = cap_to_name(i);
        ALOGI("%2u: %-20s:  %s  %s  %s\n",
            i,
            name_ptr,
            isFlagCapable(inCap, i, CAP_EFFECTIVE) ? "S" : "-",
            isFlagCapable(inCap, i, CAP_INHERITABLE) ? "S" : "-",
            isFlagCapable(inCap, i, CAP_PERMITTED) ? "S" : "-");

        cap_free(name_ptr);
    }
}

void explainFailure(cap_t badCap) {
    cap_value_t cap;
    cap_flag_value_t per_state;
    for (cap = 0; cap_get_flag(badCap, cap, CAP_PERMITTED, &per_state) != -1; cap++) {
        cap_flag_value_t inh_state, eff_state;
        cap_get_flag(badCap, cap, CAP_INHERITABLE, &inh_state);
        cap_get_flag(badCap, cap, CAP_EFFECTIVE, &eff_state);
        if ((inh_state | per_state) != eff_state) {
            char *name_ptr;
            name_ptr = cap_to_name(cap);
            ALOGE("<NOTE> Illegal cap value: %s (%s%s%s)\n",
                name_ptr,
                eff_state? "e" : "",
                inh_state ? "i" : "",
                per_state? "p" : "");
            cap_free(name_ptr);
            ALOGE("<NOTE> Under Linux, effective file capabilities must either be empty, or\n"
            "<NOTE> exactly match the union of selected permitted and inheritable bits.\n"
            "<NOTE> aka. assertEquals(i | p,  e)\n");
            break;
        }
    }
}

void getProcessCap() {
    getProcessCap(getpid());
}

void getProcessCap(pid_t inPid) {
    cap_t cap = cap_get_pid(inPid);
    if (cap == NULL) {
        ALOGE("cap_get_pid: code=%d, str=%s\n", errno, strerror(errno));
        die("cap_get_pid\n");
    }
    ALOGI("uid=%d, gid=%d, pid=%d, ppid=%d\n", getuid(), getgid(), getpid(), getppid());
    ALOGI("cap_to_text(): [%s]\n", capToString(cap).c_str());
    dumpCap(cap);
    cap_free(cap);
}

void setProcessCap(const char* inCapText) {
    ALOGI("setProcessCap(%s)\n", inCapText);
    cap_t mycaps = cap_get_pid(getpid());
    if (mycaps == NULL) {
        ALOGE("cap_get_pid: code=%d, str=%s\n", errno, strerror(errno));
        die("cap_get_pid\n");
    }
    /*
     * Raise the effective cap_setpcap.
     */
    cap_value_t capflag = CAP_SETPCAP;
    if (cap_set_flag(mycaps, CAP_EFFECTIVE, 1, &capflag, CAP_SET) != 0) {
        ALOGE("unable to manipulate CAP_SETPCAP, error=%d (%s)\n", errno, strerror(errno));
        die("cap_set_flag()\n");
    }
    //raise myself
    if (cap_set_proc(mycaps) != 0) {
        ALOGE("unable to set CAP_SETPCAP effective capability\n");
        die("cap_set_proc()\n");
    }
    //parse expected caps
    cap_t targetCap = cap_from_text(inCapText);
    if (targetCap == NULL) {
        ALOGE("cap_from_text(%s): errno=%d (%s)\n", inCapText, errno, strerror(errno));
        die("cap_from_text()");
    }

    if (0 != cap_set_proc(targetCap)) {
        ALOGE("cap_set_proc(%s), errno=%d (%s)\n", inCapText, errno, strerror(errno));
        die("cap_set_proc()\n");
    }
}

void assertRegularFile(const char* fname) {
    struct stat stbuf;
    if (0 != lstat(fname, &stbuf)) {
        ALOGE("lstat(%s) fail: errno=%d (%s)\n", fname, errno, strerror(errno));
        die("lstat()\n");
    }
    int tflag = S_ISREG(stbuf.st_mode) ? FTW_F : (S_ISLNK(stbuf.st_mode) ? FTW_SL : FTW_NS);
    if (tflag != FTW_F) {
        ALOGE("%s (Not a regular file)\n", fname);
        die("Not a regular file\n");
    }
}

void getFileCap(const char* fname) {
    assertRegularFile(fname);
    cap_t fileCap = cap_get_file(fname);
    if (fileCap == NULL) {
        if (errno != ENODATA) {
            ALOGE("Failed to get capabilities of file `%s' (%s)\n", fname, strerror(errno));
        } else {
            ALOGI("%s has no capabilities data\n", fname);
        }
        die("cap_get_file() == NULL\n");
    }

    ALOGI("%s: cap_to_text(): [%s]\n", fname, capToString(fileCap).c_str());
    dumpCap(fileCap);
    cap_free(fileCap);
}

std::string capToString(cap_t inCap) {
    std::string ret;
    char* result = cap_to_text(inCap, NULL);
    if (!result) {
        ALOGE("Failed to get capabilities of human readable format (%s)\n", strerror(errno));
        die("cap_to_text()\n");
    }
    ret = std::string(result);
    cap_free(result);
    return ret;
}

void setFileCap(const char* fname, const char* inCapText) {
    ALOGI("setFileCap(%s, %s)\n", fname, inCapText);
    assertRegularFile(fname);
    cap_t mycaps = cap_get_proc();
    if (mycaps == NULL) {
        ALOGE("unable to get current process capabilities, err=%d (%s)", errno, strerror(errno));
        die("cap_get_proc()\n");
    }
    /*
     * Raise the effective CAP_SETFCAP.
     */
    cap_value_t capflag = CAP_SETFCAP;
    if (cap_set_flag(mycaps, CAP_EFFECTIVE, 1, &capflag, CAP_SET) != 0) {
        ALOGE("unable to manipulate CAP_SETFCAP, error=%d (%s)\n", errno, strerror(errno));
        die("cap_set_flag()\n");
    }
    //raise myself
    if (cap_set_proc(mycaps) != 0) {
        ALOGE("unable to set CAP_SETFCAP effective capability\n");
        die("cap_set_proc()\n");
    }

    //parse expected caps
    cap_t targetCap = cap_from_text(inCapText);
    if (targetCap == NULL) {
        ALOGE("cap_from_text(%s): errno=%d (%s)\n", inCapText, errno, strerror(errno));
        die("cap_from_text()");
    }

    if (0 != cap_set_file(fname, targetCap)) {
        ALOGE("cap_set_file(%s), errno=%d (%s)\n", fname, errno, strerror(errno));
        explainFailure(targetCap);
        die("cap_set_file()\n");
    }
}

/*
 * 1: caps get file=X
 * 2: caps get pid=X
 * 3: caps set file=X YY
 * 4: caps set pid=X YY
 *
 * 5: caps add file=X YY
 * 6: cpas add pid=X YY
 *
 * 7: caps remove file=X YY
 * 8: cpas remove pid=X YY
 *
 * 9: caps prune file=X
 *10: cpas prune pid=X
 */
int main(int argc, char *argv[]) {
    if (argc > 1) {
        if (!strcmp(argv[1], "1")) {
            ALOGI("get file cap: [%s]\n", argv[2]);
            getFileCap(argv[2]);
        } else if (!strcmp(argv[1], "2")) {
            if (argc == 3) {
                ALOGI("get process [%s] cap\n", argv[2]);
                getProcessCap(atoi(argv[2]));
            } else {
                ALOGI("get current process cap: [%d]\n", getpid());
                getProcessCap();
            }
        } else if (!strcmp(argv[1], "3")) {
            ALOGI("set cap\n");
            ALOGI("set file cap: [%s]\n", argv[2]);
            setFileCap(argv[2], argv[3]);
        } else if (!strcmp(argv[1], "4")) {
            setProcessCap(argv[2]);
        } else {
            usage();
            ALOGE("unknown arg, %s, %s\n", *argv, *(argv+1));
        }
    } else {
        usage();
        ALOGE("unknown arg\n");
    }
    return EXIT_SUCCESS;
}
