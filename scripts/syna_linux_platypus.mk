defconfig := vs640_r_defconfig

linux_src := $(CURDIR)
linux_out := $(linux_src)/out
sdk_root := $(linux_src)/../
PATH := $(PATH):$(realpath $(sdk_root)/toolchain/aarch64/gcc-linaro-aarch64-linux-gnu-5.3/bin)
PATH := $(PATH):$(realpath $(sdk_root)/toolchain/aarch32/arm-linux-androideabi-4.9/bin)
PATH := $(PATH):$(realpath $(sdk_root)/toolchain/clang/linux-x86/clang-r383902b/bin)
PATH := $(PATH):$(realpath $(sdk_root)/toolchain/aarch64/gcc-arm-aarch64-elf-8.3/bin)
PATH := $(PATH):$(realpath $(sdk_root)/toolchain/aarch32/gcc-linaro-arm-eabi-6.2.1/bin)
PATH := $(PATH):$(realpath $(sdk_root)/toolchain/aarch32/gcc-arm-linux-gnueabi-8.3/bin)
PATH := $(PATH):$(realpath $(sdk_root)/build/tools/lib/ccache)
export PATH

CMD := LLVM=1 CC=clang ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- compiledb make

linux:
	rm -fr $(linux_out) && mkdir -p $(linux_out)
	make -C $(linux_src) mrproper
	$(CMD) -C $(linux_src) O=$(linux_out) $(defconfig)
	$(CMD) -C $(linux_src) O=$(linux_out) -j 16
	cat $(linux_out)/arch/arm64/boot/Image | lz4 -f -9 -B4 > $(linux_out)/Image.lz4

# vim:ft=make
#
