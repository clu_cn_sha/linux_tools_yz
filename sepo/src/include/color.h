#ifndef __COLOR_H__
#define __COLOR_H__

#define HEADER  "\033[22;36m"
#define OKBLUE "\033[22;34m"
#define OKGREEN "\033[22;32m"
#define WARNING "\033[01;33m"
#define FAIL "\033[22;31m"
#define ENDC "\033[0m"

#endif // __COLOR_H__
