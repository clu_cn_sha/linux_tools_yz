#include "SepoArgv.h"

static std::string commands[] = {
    "none",
    "help",
    "command",
    "version",
    "check-ancestor"
};

static struct option LONG_OPTIONS[] = {
    {"ancestor", no_argument, 0, 'a' },
    { "help", no_argument, 0, 'h' },
    { "version", no_argument, 0, '(' },
    {"verbose", no_argument, 0, 'v' },
    { "command", required_argument, 0, 'c' },
    { "jobs", required_argument, 0, 'j' },
    { "profile", required_argument, 0, 'p' },
    { 0, 0, 0, 0 }
};

SepoArgv::SepoArgv()
:funcSelected(0), jobs(0), bDebug(false)
{
}

bool SepoArgv::parse(int argc, char** argv) {
    char c;
    bool ret = false;
    try {
        while(1) {
            int option_index = 0;
            c = getopt_long(argc, argv, "ahvc:j:p:", LONG_OPTIONS, &option_index);
            /* detect end of the options */
            if (-1 == c) {
                break;
            }

            switch (c) {
            case 0:
                /* If this option set a flag, do nothing else now. */
                if(0 != LONG_OPTIONS[option_index].flag) break;
                if ( bDebug ) {
                    printf("option %s", LONG_OPTIONS[option_index].name);
                    if (optarg) {
                        printf(" with arg %s\n", optarg);
                    }
                }
                break;
            case 'h':
                if ( bDebug ) {
                    printf("-h\n");
                }
                this->set_func(COMMANDS_HELP);
                break;
            case 'v':
                if ( bDebug ) {
                    printf("-v\n");
                }
                break;
            case 'c':
                if ( bDebug ) {
                    printf("-c %s\n", optarg);
                }
                this->set_func(COMMANDS_COMMAND);
                this->theCmd = std::string(optarg);
                break;
            case 'p':
                if ( bDebug ) {
                    printf ( "-p %s\n", optarg );
                }
                this->theProfile = std::string(optarg);
                break;
            case 'a':
                if ( bDebug ) {
                    printf("-a\n");
                }
                this->set_func(COMMANDS_ANCESTOR);
                break;
            case '(':
                if ( bDebug ) {
                    printf("-(\n");
                }
                this->set_func(COMMANDS_VERSION);
                break;
            case 'j':
                printf("-j %s\n", optarg);
                this->jobs = atoi(optarg);
                break;
            case '?':
                return false;
                break;
            default:
                printf("Unknown option:%d\n", c);
                return false;
            }
        }

        if (!this->funcSelected) {
            if (bDebug) {
                printf ("No functions selected\n" );
            }
            return false;

        }

        if (2 == this->funcSelected) {
            if (0 == this->theProfile.size()) {
                fprintf (stderr, "using default repo project.list");
            } else {
                //pass
            }
        }

        /* Print any remaining command line arguments (not options). */
        if(optind < argc) {
            while(optind < argc) {
                theArgs.push_back(argv[optind++]);
            }
        }
        ret = true;
    } catch (std::string *e) {
        fprintf(stderr, "Error: %s\n", e->c_str());
    } catch (std::exception &e) {
        fprintf(stderr, "Error: %s\n", e.what());
    } catch (...) {
        fprintf(stderr, "Error: unknown\n");
    }
    return ret;
}

void SepoArgv::set_func(int i) {
        if(this->funcSelected) {
            fprintf(stderr, "conflict options : %s vs %s\n", commands[i].c_str(),
                    commands[funcSelected].c_str());
            throw new std::string("invalid_func_args");
        }
        else {
            funcSelected = i;
        }
}

void SepoArgv::print_usage() {
    printf("Usage: sepo [OPTION] {-c command | -a commitA commitB}\n \
sepo is a tool for managing repo/git\n \
OPTIONS:\n \
    -j n | --jobs n     run commands in n threads, only available for -c \n \
    -h   | --help       show this help info\n \
    -v   | --verbose    show verbose info, -v can be used more than once. eg: -vv, -vvv\n \
    -a   | --ancestor   check ancestor-descendant relationshiop\n \
         | --version    show version info\n");
}


#ifdef SEPOARGV_MODULE_TEST
int main(int argc, char** argv) {
    int ret = 99;
    SepoArgv *pArgs = new SepoArgv();
    bool bParsed = pArgs->parse(argc, argv);
    if(bParsed) {
        printf("\nParse Success!\n");
    } else {
        printf("\nParse Fail!\n");
    }

    switch (pArgs->funcSelected) {
        case COMMANDS_NONE:
        case COMMANDS_HELP:
            pArgs->print_usage();
            ret = EXIT_SUCCESS;
            break;
        case COMMANDS_VERSION:
            printf ( "COMMANDS_VERSION\n" );
            break;
        case COMMANDS_COMMAND:
            printf ( "command\n" );
            break;
        case COMMANDS_ANCESTOR:
            printf ( "ancestor\n" );
            if (2 != pArgs->theArgs.size()) {
                printf ( "sepo -a accepts exactly 2 parameters\n" );
                ret = 1;
            } else {
                printf ( "%s -- %s\n", pArgs->theArgs[0].c_str(), pArgs->theArgs[1].c_str());
            }
            break;
        default:
            pArgs->print_usage();
            break;
    }

    return ret;
}
#endif
