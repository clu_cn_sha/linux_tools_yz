#include <cstdlib>
#include <cstdio>
#include <string>
#include <cstring> //strcmp
#include <sstream>
#include <iostream>
#include <fstream> //std::ofstream
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <getopt.h>
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include "GIT.h"
#include "wrapper.h"
#include "CMDrunner.h"

using namespace std;

static bool bVerbose = false;
static bool bDebug = false;
vector<string> theArgs;
string theProfile = "v4";
static struct option LONG_OPTIONS[] = {
    //these options set flags
    //these options do not flag
    { "verbose", no_argument,       0,        'v' },
    { "help",    no_argument,       0,        'h' },
    { "version", no_argument,       0,        '('   },
    //these options require arguments
    { "profile", required_argument, 0,        'p' },
    { 0, 0, 0, 0 }
};

void help(char** argv) {
    printf ( "Usage:\n%s <BASE_TAG> <TARGET_TAG> [-v | --verbose]\n", argv[0]);
}

bool parser(int argc, char** argv) {
    char c;
    while(1) {
        int option_index = 0;
        c = getopt_long(argc, argv, "vh(p:", LONG_OPTIONS, &option_index);
        /* detect end of the options */
        if (-1 == c) {
            break;
        }

        switch (c) {
        case 0:
            /* If this option set a flag, do nothing else now. */
            if(0 != LONG_OPTIONS[option_index].flag) break;
            if ( bDebug ) {
                printf("option %s", LONG_OPTIONS[option_index].name);
                if (optarg) {
                    printf(" with arg %s\n", optarg);
                } else {
                    printf ( "\n" );
                }
            }
            break;
        case '(':
            if ( bDebug ) {
                printf("--version\n");
            }
            goto exit_version;
            break;
        case 'h':
            if ( bDebug ) {
                printf("-h\n");
            }
            goto exit_help;
            break;
        case 'v':
            if ( bDebug ) {
                printf("-v\n");
            }
            bVerbose = true;
            break;
        case 'p':
            if ( bDebug ) {
                printf ( "-p %s\n", optarg );
            }
            theProfile = string(optarg);
            break;
        case '?':
            goto exit_help;
            break;
        default:
            printf("Unknown option:%d\n", c);
            goto exit_help;
        }
    }

    /* Print any remaining command line arguments (not options). */
    if(optind < argc) {
        while(optind < argc) {
            theArgs.push_back(argv[optind++]);
        }
        if (2 == theArgs.size()) {
            return true;
        }
    }

exit_help:
    help(argv);
    return false;

exit_version:
    printf ( "sbtlog " SBTLOG_VERSION "\n" );
    return false;
}

int main(int argc, char** argv) {
    if (!parser(argc, argv)) {
        return EXIT_FAILURE;
    }
    int ret = 99;
    int aCommits = 0;
    int aChangedGits = 0;
    GIT aWorker;
    CMDrunner aRunner;
    std::stringstream aSumHeader;
    std::stringstream aSumComments;
    aSumComments << "\nDetail Comments:\n";
    std::string argv1 = theArgs[0];
    std::string argv2 = theArgs[1];

    std::string pwd;
    getcwd(&pwd);
    std::vector<std::string> aProjectList = readProfile(theProfile);
    for (vector<string>::iterator it = aProjectList.begin(); it != aProjectList.end(); it++) {
        chdir(pwd.c_str());
        if (chdir((*it).c_str())) {
            fprintf ( stderr, "chdir(%s) fail\n", (*it).c_str() );
            exit(6);
        }
        int aRet = aWorker.check_ancestor(argv1, argv2);
        std::cout << "  " << *it << std::endl;
        switch (aRet) {
            case 0: {
                //good
                break; }
            case 1: {
                //good
                std::stringstream ss;
                std::string theCmd;
                ss << "git rev-list " << argv1 << ".." << argv2 << " | wc -l";
                theCmd = ss.str();
                aRunner.run(theCmd);
                aChangedGits ++;
                aCommits += std::stoi(aRunner.getOutput());

                aSumComments << "--------------------------------------------------\n";
                aSumComments << "   " << *it << "\n";
                aSumComments << "--------------------------------------------------\n";
                ss.str("");
                ss << "git --no-pager log " << argv1 << ".." << argv2;
                if (bVerbose) {
                    ss << " --name-status";
                } else {
                    ss << " --oneline";
                }
                theCmd = ss.str();
                aRunner.run(theCmd);
                aSumComments << aRunner.getOutput();
                break; }
            default: {
                //bad
                std::cerr << "[ERROR] " << argv1 << " is not based on " << argv2 << std::endl;
                exit(aRet); }
        }
    }

    aSumHeader << "Delta: " << argv1 << ".." << argv2 << "\n";
    aSumHeader <<  "Changed Gits: " << aChangedGits << " / " << aProjectList.size() << "\n";
    aSumHeader << "Total Commits: " << aCommits << "\n";

    chdir(pwd.c_str());
    std::ofstream outfile("log.log");
    if (outfile.is_open()) {
        outfile << aSumHeader.str();
        outfile << aSumComments.str();
        outfile.close();
    } else {
        std::cout << "[ERROR] can not open output file\n";
    }
#if 0
    std::cout << aSumHeader.str();
    if (aCommits) {
        std::cout << "\nDetail Comments:\n" << aSumComments.str() << std::endl;
    }
#endif

    return ret;
}
