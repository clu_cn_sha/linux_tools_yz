#include "Ant.h"
#include <string>
#include <cstring>
#include <vector>
#include <csignal>
#include <thread> //c++11 thread
#include <mutex> //c++11 mutex
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fstream>
#include <iostream>
#include <sys/stat.h>
#include <fcntl.h>

#include "wrapper.h"

std::mutex mutexProgress;
static int gProgress;
static int gGrandTotalDirs;

void suicide() {
    fprintf ( stderr, "[x] God !\n");
    kill(0, SIGKILL);//send SIGKILL to all processes in current process group
}

/* 
 * change argv: std::string-->char** 
 */
static char** new_argv(std::vector<std::string> inStrs, int inStart=0)
{
    char** argv = new char*[inStrs.size() + 1 - inStart];
    std::vector<std::string>::iterator it;
    for(int i=inStart; i<=inStrs.size(); i++) {
        argv[i-inStart] = const_cast<char*>(inStrs[i].c_str());
    }
    argv[inStrs.size()-inStart] = 0;

//    printf ( "new_argv[%s][%s]\n", *argv, *(argv+1));

    return argv;
}

static void del_argv(char** argv)
{
    delete[] argv;
}

/* 
 * using basic algorithm to divide jobs into groups
 *
 * */
std::vector<int> slicer(int job, int slice, int verbose) {
    std::vector<int> result;
    std::string rettips;
    int i, avg, remain, load;
    slice = job<slice ? job : slice;
    slice = slice < 1 ? 1 : slice;
    rettips = "job=" + std::to_string(job) + ",slice=" + 
        std::to_string(slice) + ": ";//C++11 std::to_string() in <string>
    avg = job/slice;
    remain = job-slice*avg;
    rettips += "avg=" + std::to_string(avg) + ", remain=" + std::to_string(remain) + " | ";
    for(i = 0; i< slice; i++) {
        load = avg + (remain/(i+1) ? 1 : 0);
        result.push_back(load);
        rettips += std::to_string(load);
        rettips += " ";
    }
    if (verbose>1) {
        printf ( "%s\n", rettips.c_str());
    }
    return result;
}

bool run_cmd_in_dir(std::string inWorkdir, char* const* inArgv)
{
    int status;
    pid_t has_child, w_pid;
    const char* aWorkdir = inWorkdir.c_str();
    int currentJobID;

    try {
        mutexProgress.lock();
        gProgress++;
        currentJobID = gProgress;
        mutexProgress.unlock();
        has_child = fork();
        switch (has_child) {
            case -1:
                perror("a job_runner fork");
                throw std::string("fork error");
            case 0:                //this is child
                printf ( "[ %02d%% ] %s\n", 100*currentJobID/gGrandTotalDirs, aWorkdir);
                if(chdir(aWorkdir)) {
                    fprintf (stderr, "chdir(%s) fail. Why: %s\n", aWorkdir, strerror(errno));
                    throw std::string("chdir() fail");
                }
                { // do not open controlling terminal, just pick some usable for debugging output
                    int fd;
                    if (open("/dev/console", O_RDWR) < 0) {
                        fd = open("/dev/null", O_RDWR);
                    }
                    dup2(fd, 0);
                    dup2(fd, 1);
                    dup2(fd, 2);
                    close(fd);
                }
                execvp(*inArgv, inArgv);
                fprintf(stderr, "[] exec() fail, Cause:%s \n", strerror(errno));
                throw std::string("exec() fail");
            default:               //this is parent
                w_pid = waitpid(has_child, &status, WUNTRACED | WCONTINUED);
                if(w_pid == -1) {
                    fprintf(stderr, "[] waitpid(%d) failed, Cause: %s\n", has_child, strerror(errno));
                    return false;
                }

                if(WIFEXITED(status)) {
                    //suicide
                    if(WEXITSTATUS(status)) {
                        printf("[ERROR] Child<%d> exited, status=%d\n", has_child, WEXITSTATUS(status));
                        return false;
                    } else {
//                        printf("[ OK  ] Child<%d> exited, status=%d\n", has_child, WEXITSTATUS(status));
                    }
                }
                else if(WIFSIGNALED(status)) {
                    printf("[] Child<%d> killed by signal %d\n", has_child, WTERMSIG(status));
                }
                else if(WIFSTOPPED(status)) {
                    printf("[] Child<%d> stopped by signal %d\n", has_child, WSTOPSIG(status));
                } else {
                    printf("[] Child<%d> exited with weird status %0x08x\n", has_child, status);
                }
                break;
        }                       /* -----  end switch  ----- */

//        printf("[]LEAVE\n");
        return true;

    }
    catch (const std::string *e) {		/* handle exception: */
        fprintf(stderr, "[]exit abnormally\n");
        printf("[]LEAVE\n");
        return false;
    }
    catch (...) {		/* handle exception: unspecified */
        printf ( "unknown error\n" );
        return false;
    }

}

void *run_cmd_in_dirs(void *in_args)
{
    Job_args *args = (Job_args *) in_args;
    int verbose = 2;
    /* argument-string to argumemt-array */
    std::vector<std::string> argList = cut(args->cmd, " ");
    std::string prog = argList[0];
    char** aArgv = new_argv(argList, 0);

    for(std::vector<std::string>::iterator it = args->dirs.begin(); it != args->dirs.end(); it++) {
        std::string curWorkdir = args->basedir + "/" + *it;
        if(! run_cmd_in_dir(curWorkdir, aArgv)) {
            printf("[ERROR] run cmd \"%s\" failed in \"%s\"\n", args->cmd.c_str(), curWorkdir.c_str());
            suicide();
        }
    }

    del_argv(aArgv);
    return NULL;
}

void run_cmd_container(std::string inCmd, std::vector<std::string> inSubDirs, int inJobs)
{
    std::vector<int> slices = slicer(inSubDirs.size(), inJobs, 0);
    gGrandTotalDirs = inSubDirs.size();
    int slice = slices.size();
//    printf ( "slice=%d\n", slice );
//    pthread_t theThreads[slice];
    std::thread theThreads[slice];
    Job_args theJobArgs[slice];
    Job_args *pJobArgs[slice];
    std::string pwd;
    getcwd(&pwd);

    for (int i=0, cursor=0; i<slice; i++) {
        //generate args for run_cmd_in_dirs()
        pJobArgs[i] = &theJobArgs[i];
        theJobArgs[i].id = i;
        theJobArgs[i].basedir = pwd;
        theJobArgs[i].cmd = inCmd;
        for (int count=0; count < slices[i]; count++) {
            theJobArgs[i].dirs.push_back(inSubDirs[cursor++]);
        }

        theThreads[i] = std::thread(run_cmd_in_dirs, pJobArgs[i]);
//        //start run_cmd_in_dirs
//        int rc = pthread_create(&theThreads[i], NULL, run_cmd_in_dirs, (void *) (pJobArgs[i]));
//        if(rc) {
//            printf("Create thread error \n");
//            exit(EXIT_FAILURE);
//        } else {
//            printf ( "add 1 more thread\n" );
//        }
    }

    for(int i = 0; i < slice; i++) {
//        pthread_join(theThreads[i], NULL);
        theThreads[i].join();
//        printf("thread #%d joined.\n", i);
    }

}

#ifdef ANT_MODULE
int main ( int argc, char *argv[] )
{
    printf ( "\n=====slicer====\n" );
    slicer(3, 0, 2);
    slicer(3, 1, 2);
    slicer(3, 2, 2);
    slicer(3, 3, 2);
    std::vector<int> sRet = slicer(3, 9, 2);
    for(std::vector<int>::iterator it = sRet.begin(); it != sRet.end(); it ++) {
        printf ( "%d\n", *it);
    }

    printf ( "\n=====run_cmd_in_dir====\n" );
    char* aArgv[3] = {"ls", "/", 0};
    run_cmd_in_dir("/",  aArgv);

    printf ( "\n=====run_cmd_in_dirs====\n" );
    Job_args aArg, *pArg;
    pArg = &aArg;
    aArg.id = 1;
    aArg.basedir = "/home/yu";
    pArg->dirs.push_back("temp");
    pArg->dirs.push_back("r");
    pArg->dirs.push_back("workspace");
    aArg.cmd = "ls";
    run_cmd_in_dirs((void*)pArg);

    printf ( "\n=====run_cmd_container====\n" );
    std::vector<std::string> job_dirs;
    job_dirs.push_back("bootable/recovery");
    job_dirs.push_back("build");
    job_dirs.push_back("cts");
    job_dirs.push_back("development");

    run_cmd_container("git gc --aggressive --prune=now", job_dirs, 2);


    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
#endif // ANT_MODULE
