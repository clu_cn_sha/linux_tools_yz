#include "GIT.h"
#include "color.h"
#include "CMDrunner.h"
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

//#ifdef __cplusplus
//extern "C" {
//#endif
//
//// all of your legacy C code here
//
//#ifdef __cplusplus
//}
//#endif
//}

#include <stdio.h>
#include <string>
#include <string.h>
#include <cstdlib>
#include <vector>

GIT::GIT()
:version("")
{
}

std::string GIT::get_version()
{
    std::string ret;
    if (!this->version.empty()) {
#ifdef GIT_WRAPPER_MODULE
        printf ( "version ready, return\n" );
#endif
        ret = this->version;
        return ret;
    }

#ifdef GIT_WRAPPER_MODULE
    printf ( "checking version ...\n" );
#endif
    std::string s = "";
    CMDrunner aRunner;
    std::string aCmd = "git --version";
    aRunner.run(aCmd);
    char *saveptr, *pch, *aOutStr;
    char delim[] = " ";
    saveptr = NULL;
    pch = NULL;
    aOutStr = strdup(aRunner.getOutput().c_str());
    pch = strtok_r(aOutStr, delim, &saveptr);
    while(pch) {
        if(*pch >= '0' && *pch <= '9') {
            s += pch;
            if((*(pch + strlen(pch) -1) == '\n')||(*(pch + strlen(pch) -1) == '\r')) {
                s.pop_back();//needs -std=c++11
            } else {
                //pass
            }
        }
        pch = strtok_r(NULL, delim, &saveptr);
    }
    this->version = s;
    ret = s;
#ifdef GIT_WRAPPER_MODULE
    printf ( "git version =[%s]\n", this->version.c_str());
#endif

    return ret;
}

short GIT::verify_git_version(std::string minGitVersion)
{
    short ret = 99;
    char *pch, *saveptr;
    char *pch2, *saveptr2;
    char delim[] = ".";
    char *min_ver;
    char* cur_ver_bk = strdup(this->get_version().c_str());
    min_ver = strdup(minGitVersion.c_str());
    pch = strtok_r(cur_ver_bk, delim, &saveptr);
    pch2 = strtok_r(min_ver, delim, &saveptr2);
    while(pch && pch2) {
        if (atoi(pch) < atoi(pch2)) {
            ret = 1;
            break;
        }
        if (atoi(pch) > atoi(pch2)) {
            ret = 0;
            break;
        }
        pch = strtok_r(NULL, delim, &saveptr);
        pch2 = strtok_r(NULL, delim, &saveptr2);
    }

#ifdef GIT_WRAPPER_MODULE
    if(ret) {
        printf ("git version %s < %s\n", this->version.c_str(), minGitVersion.c_str());
    } else {
        printf ( "git version %s OK. (>=%s)\n", this->version.c_str(), minGitVersion.c_str());
    }
#endif

    return ret;
}

short GIT::check_ancestor(std::string inCommitA, std::string inCommitB)
{
    int retA, retB;
    int ret = 99;
    CMDrunner aRunner;
    aRunner.run("git merge-base --is-ancestor " + inCommitA + " " + inCommitB);
//    aRunner.dump();
    retA = aRunner.getRet();
    aRunner.run("git merge-base --is-ancestor " + inCommitB + " " + inCommitA);
    retB = aRunner.getRet();

    if (0 == retA && 0 == retB) {
        printf ( "%s[%s %s %s] %s=== %s[%s %s %s]%s", HEADER, ENDC, inCommitA.c_str(), HEADER, OKGREEN, HEADER, ENDC, inCommitB.c_str(), HEADER, ENDC);
        ret = 0;
    } else if (0 == retA && 1 == retB) {
        printf ( "%s[%s %s %s] %s--> %s[%s %s %s]%s", HEADER, ENDC, inCommitA.c_str(), HEADER, OKGREEN, HEADER, ENDC, inCommitB.c_str(), HEADER, ENDC);
        ret = 1;
    } else if (1 == retA && 0 == retB) {
        printf ( "%s[%s %s %s] %s<-- %s[%s %s %s]%s", HEADER, ENDC, inCommitA.c_str(), HEADER, OKGREEN, HEADER, ENDC, inCommitB.c_str(), HEADER, ENDC);
        ret = 2;
    } else if (1 == retA && 1 == retB) {
        printf ( "%s[%s %s %s] %s-X- %s[%s %s %s]%s", HEADER, ENDC, inCommitA.c_str(), HEADER, WARNING, HEADER, ENDC, inCommitB.c_str(), HEADER, ENDC);
        ret = 3;
    } else {
        printf ( "%s[%s %s %s] %sERR %s[%s %s %s]%s", HEADER, ENDC, inCommitA.c_str(), HEADER, FAIL,    HEADER, ENDC, inCommitB.c_str(), HEADER, ENDC);
        ret = 4;
    }// end of if/else/xxx

    return ret;
}


#ifdef GIT_WRAPPER_MODULE
int main ( int argc, char *argv[] )
{
    GIT aGit;
    aGit.get_version();
    aGit.verify_git_version(MIN_GIT_VERSION);
    aGit.check_ancestor("a", 
        "97c8bef81d1febc8e2594a52bd4e7d942e333219");
    printf ( "\n" );
    aGit.check_ancestor("97c8bef81d1febc8e2594a52bd4e7d942e333219", 
        "97c8bef81d1febc8e2594a52bd4e7d942e333219");
    printf ( "\n" );

    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
#endif
