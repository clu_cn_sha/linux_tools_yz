LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)
LOCAL_SRC_FILES := \
	android/mem_see.cpp \
	subprocess/CMDrunner.cpp
LOCAL_MODULE := me_see
LOCAL_MODULE_TAGS := optional
LOCAL_CPPFLAGS += -D_ANDROID_ -D__LINUX__
LOCAL_CFLAGS := -std=c++11
LOCAL_LDFLAGS +=

LOCAL_SHARED_LIBRARIES := \
	liblog \
	libutils \
	libcutils \
	libstlport

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH)/subprocess \
	bionic \
	external/stlport/stlport

#LOCAL_MODULE_PATH := $(TARGET_OUT_OPTIONAL_EXECUTABLES)
#LOCAL_MODULE_PATH := $(TARGET_OUT_VENDOR_SHARED_LIBRARIES)
LOCAL_MODULE_PATH := $(TARGET_OUT_VENDOR_EXECUTABLES)

include $(BUILD_EXECUTABLE)
