package ba

import com.fasterxml.jackson.annotation.JsonProperty

data class AndroidBuildConfig(
        var pre: MutableList<PrepareCode> = mutableListOf(),
        var build: BuildCode? = null
)

data class BuildCode(
        var workDir: String = ".",
        var buildCmd: String = "make",
        var args: List<String>? = null
)

data class PrepareCode(
        var workDir: String = "",
        var initCmd: String? = null,
        var bInitAndSync: Boolean = false,
        var bClean: Boolean = false,
        var cherryPicks: MutableList<String> = mutableListOf(),
        var postCmd: MutableList<String> = mutableListOf()
)