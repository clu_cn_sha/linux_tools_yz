sign_key := ppdai.pem
sign_pub_key := ppdai.pub.pem
file_to_sign := file
file_signature := file.sig
sign:
	openssl rsautl -sign -inkey $(sign_key) -out $(file_signature) -in $(file_to_sign)
verify:
	openssl rsa -in $(sign_key) -pubout -out $(sign_pub_key)
	openssl rsautl -verify -inkey $(sign_pub_key) -in $(file_signature) -pubin
# sign & verify
sv:
	openssl dgst -sha1 -sign $(sign_key) -out $(file_signature) $(file_to_sign)
	openssl base64 -in $(file_signature) -out $(file_signature).b64
	openssl dgst -sha1 -verify $(sign_pub_key) -signature $(file_signature) $(file_to_sign)
